package com.vseravno.truereleaseradar;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Configuration
public class ApiBuilder {
private static final Logger logger = LoggerFactory.getLogger(ApiBuilder.class);
  private static final String CLIENT_ID = "6da454168fac468890fa4c3effa49e25";

  private final YAMLConfig yamlConfig;

  @Autowired
  public ApiBuilder(YAMLConfig yamlConfig) {
    this.yamlConfig = yamlConfig;
  }

  @Bean
  public SpotifyApi getApi() {
    try {
      SpotifyApi api =
          new SpotifyApi.Builder()
              .setClientId(CLIENT_ID)
              .setClientSecret(yamlConfig.getClientSecret())
              .setRefreshToken(yamlConfig.getRefreshToken())
              .build();
      AuthorizationCodeCredentials tokens = api.authorizationCodeRefresh().build().execute();
      api.setAccessToken(tokens.getAccessToken());
      return api;
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException();
    }
  }
}
