# Release Radar

Release Radar is a Spotify plugin.
It adds all the new releases from all the artists user is following to a private playlist.

## Tech Radar 
* Java
* Spring Boot
* PostgreSQL
* CI/CD via gitlab: 
   * automated build
   * Docker containerization
   * deployment in Kubernetes

## Demo video
![](releaseradar.mp4)